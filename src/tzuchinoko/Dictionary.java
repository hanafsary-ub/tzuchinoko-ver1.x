package tzuchinoko;

import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 取得した文字列を形態素解析してマルコフ辞書へ登録する学習機能と
 * マルコフ辞書から一定の規則に従って新たに文章を生成する発言機能の大部分を担うクラス
 * 
 * 学習の際の使用例
 * new Dictionary( "ツイート元の文章です" );
 * 
 * 文章生成の際の使用例
 * String remark = new Dictionary().chain();
 * 
 * @author hanafusaryo
 *
 */
public class Dictionary extends DataBaseConnection {
    /* ログ出力を行うインスタンス */
    private static final Logger log = LoggerFactory.getLogger( Dictionary.class );
    /* SQL文を実行するSQLステートメントハンドラ */
    private Statement stmt = null;
    
    /**
     * デフォルトコストラクタ
     */
    public Dictionary(){
    }
    /**
     * 形態素解析と学習を行うコンストラクタ
     * @param message 学習元の文字列 
     * @throws SQLException
     */
    public Dictionary( String message ) throws SQLException{
        MorphologicalAnalysis ma = new MorphologicalAnalysis();
        study( ma.analyze( message ) );
    }
    /**
     * 形態素解析されたリストをマルコフ連鎖に従ってデータベースに登録する
     * @param morphemes MorphologicalAnalysis#analyze(String)によって形態素解析された文字列のリスト
     * @throws SQLException
     */
    public void study( List<String> morphemes ) throws SQLException{
        try{
            // データベースに接続する
            connect( getUserName(), getPassword() );
            // ステートメントを生成する
            stmt = conn.createStatement();
            String sql = "INSERT INTO markov_table( id, prefix1, prefix2, suffix, head ) VALUES( ?, ?, ?, ?, ? );";
            // 繰り返す使うSQL文を事前にコンパイル
            PreparedStatement pstmt = conn.prepareStatement( sql );
            // id を求めるために辞書のデータ数を取得する
            ResultSet rs = stmt.executeQuery( "SELECT id FROM markov_table ORDER BY id DESC LIMIT 1;" );
            int id = ( rs.next() )? rs.getInt( 1 ) + 1 : 0;
            String pre1, pre2, suf; // 接頭語1,接頭語2,接尾語
            int head, i = 0;
            do{
                // 文頭であればheadを1そうでなければ0とする
                head = ( i == 0 )? 1: 0;
                pre1 = (String)( morphemes.get( i++ ) );
                // prefix1が[end]なら、つまり元の文章が空文字列なら学習をせずに終了する
                if( pre1.equals( "[end]" ) ) break;

                pre2 = (String)( morphemes.get( i++ ) );
                // prefix2に[end]が来たら代わりに[empty]を入れ、suffixに[end]を入れる
                if( pre2.equals( "[end]" ) ){
                    pre2 = "[empty]";
                    suf = "[end]";
                }else{
                    suf  = (String)( morphemes.get( i-- ) );
                }
                // データの挿入
                pstmt.setInt( 1, id ); pstmt.setString( 2, pre1 ); pstmt.setString( 3, pre2 ); pstmt.setString( 4, suf ); pstmt.setInt( 5, head );
                pstmt.addBatch();
                id++;
            }while( !suf.equals( "[end]" ) ); // suffixが[end]なら登録処理を終了させる。
            // addBatch()でまとめておいたSQL文をまとめて実行
            pstmt.executeBatch();
            // データベースの接続を閉じる
            stmt.close();
            pstmt.close();
            disconnect();
        }catch( SQLException e ){ 
            log.error( "データベースに接続できません」。", e );
            if( conn != null ){
                conn.rollback();
                conn.close();
            }
        }
    }
    /**
     * データベースから３次マルコフ連鎖に従って文章を作成する
     * @return 作成された文章の文字列
     * @throws SQLException
     */
    public String chain() throws SQLException{
        //log.info( "Dictionary#chain()が呼び出されました" );
        String message = "";
        try{
            boolean head = true;
            String pre1, pre2 = "", suf = "";
            ResultSet mrkv;
            // データベースに接続する
            //log.info( "データベースに接続します。" );
            connect( getUserName(), getPassword() );
            //log.info( "データベースに接続出来ました。" );
            //String sql1 = "SELECT * FROM markov_table WHERE head = 1 AND id IN( SELECT MIN(id) FROM markov_table GROUP BY prefix1, prefix2, suffix );";
            // dependent subqueryが遅いのでprefix1,prefix2,suffixが重複してもいいようにする
            String sql1 = "SELECT * FROM markov_table WHERE head = 1;";
            String sql2 = "SELECT * FROM markov_table WHERE prefix1=? AND prefix2=?;";
            // あらかじめコンパイルされたSQL文を用意( TYPE_SCROLL_INSENSITIVEを指定してResultSetを順方向以外にも辿れるようにする )
            //PreparedStatement ps1 = conn.prepareStatement(sql1, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
            PreparedStatement ps2 = conn.prepareStatement(sql2, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
            stmt = conn.createStatement();
            //log.info( "文章生成の準備が完了しました。" );
            do{
                if( head ){
                    // マルコフ辞書から先頭要素であり、prefix1とprefix2の組み合わせ(以下keyとする)の重複を排除したものをResultSetに格納する
                    mrkv = stmt.executeQuery( sql1 );
                    //mrkv = ps1.executeQuery();
                    if( !mrkv.next() ){
                        log.error( "辞書に何も入っていません。");
                        break;
                    }
                }else{
                    // 取得したsuffixに一致するprefix1をもつものを探す
                    ps2.setString( 1, pre2 );
                    ps2.setString( 2, suf );
                    mrkv = ps2.executeQuery();
                }
                // セットの最後の行に移動し、行番号を取得すれば取得したセットのサイズを求めることができる
                // Resultセットの添字は1から数えるため最後の行=サイズとなる
                mrkv.last(); int size = mrkv.getRow();
                // 取得したセットの中からランダムに１つのデータを選択する
                mrkv.absolute( (int)(Math.random() * size) + 1 );
                pre1 = mrkv.getString( "prefix1" ); pre2 = mrkv.getString( "prefix2" ); suf = mrkv.getString( "suffix" );
                //log.info( "トークン:[" + pre1 + "],[" + pre2 + "],[" + suf +"]" );
                // 空文字列""が辞書登録される場合、prefix1に"[end]"が入り,suffixに"[end]"が来なくなるため無限ループになってしまう。以下の条件分岐は応急処置
                if( pre1.equals( "[end]" ) ) break;
                if( head ){
                    message = ( pre2.equals( "[empty]" ) )? pre1 : pre1 + pre2;
                    if( !suf.equals( "[end]" ) ) message += suf;
                    head = false;
                }else{
                    if( !suf.equals( "[end]" ) ) message += suf;
                }
            }while( !suf.equals("[end]") );// suffixが終端文字だったら処理を完了する
            //log.info( "文章生成処理を終了します。" );
            stmt.close();
            ps2.close();
            disconnect();
        }catch( SQLException e ){
            log.error( "データベースに接続できません。", e );
            if( conn != null ){
                conn.rollback();
                conn.close();
            }
        }
        // 改行タグ[nextLine]を改行文字'\n'に置換
        String regex = "\\[nextLine\\]";
        Pattern p = Pattern.compile( regex );
        Matcher m = p.matcher( message );
        message = m.replaceAll( "\n" );
        //log.info( "文章が生成されました。" );
        return message;
    }
    /**
     * 生成された文章が学習元ツイートと同様でないか判断する。
     * 実際の学習内容はリプライ文字やURLなどは除外されて学習するため、引数の文字列で学習元ツイートを検索し、
     * 検索結果が出れば一致したものとみなす
     * @param subject 生成された文章の文字列
     * @return 同一であればfalse, 異なっていればtrueを返す
     * @throws SQLException
     */
    public boolean isOriginal( String subject ) throws SQLException{
        boolean original = false;
        String sql = "SELECT * FROM tweets WHERE tweet LIKE ?;";
        try{
            connect( getUserName(), getPassword() );
            PreparedStatement ps = conn.prepareStatement( sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY );
            ps.setString( 1, "%" + subject + "%" );
            ResultSet rs = ps.executeQuery();
            if( !rs.next() ){
                original = true;
            }else{
                log.debug( "生成された文字列[ " + subject + " ]は学習元のツイートと同一の可能性があります。" );
            }
            disconnect();
        }catch( SQLException e ){
            log.error( "データベースに接続できません。", e );
            if( conn != null ){
                conn.rollback();
                conn.close();
            }
        }
        return original;
    }
}
