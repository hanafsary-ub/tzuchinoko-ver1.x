package tzuchinoko.jobs;

import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import tzuchinoko.DataBaseConnection;

/**
 * Quartz Schedulerで定期実行するプロフィール更新クラス
 * @author hanafusaryo
 *
 */
public class UpdateProfileJob extends DataBaseConnection implements Job{
	/* ログ出力を行うインスタンス */
	private static final Logger log = LoggerFactory.getLogger( UpdateProfileJob.class );
	/* Twitterに接続するのに必要 */
	private static final Twitter twitter = new TwitterFactory().getInstance();
	/* プロパティファイルから設定を読み込むために必要 */
	private static final Properties prop = new Properties();
	/* ツイッターアカウントの表示名 */
	private String profileName;
	/* URL */
	private String url;
	/* 出身地 */
	private String location;
	/* プロフィールの詳細 */
	private String description;
	/**
	 * プロフィール情報の読み込み初期設定
	 * @throws TwitterException
	 */
	public UpdateProfileJob() throws TwitterException{
		this.setProfileData();
	}
	/**
	 * プロフィールをprofile.propertiesから読み込む
	 */
	public void setProfileData(){
		try {
			prop.load( new FileInputStream( "./src/profile.properties" ) );
		} catch (IOException e) {
			log.error( "profile.propertiesからプロフィール情報に関する設定が読み込めません", e );
		}
		this.profileName = prop.getProperty( "profile.name" );
		this.url = prop.getProperty( "profile.url" );
		this.location = prop.getProperty( "profile.location" );
		this.description = prop.getProperty( "profile.description.basic" );
	}
	/**
	 * ツイッターにプロフィール情報を更新する
	 */
	public void updateProfile(){
		try {
			twitter.updateProfile( this.profileName, this.url, this.location, this.addLearned( this.description ) );
		} catch (TwitterException e) {
			log.error( "Twitterにプロフィール情報を更新できませんでした。", e );
		} catch (SQLException e) {
			log.error( "データベースに接続できませんでした。", e );
		}
	}
	/**
	 * データベースから学習ツイート数を取得する
	 * @return 学習ツイート数
	 * @throws SQLException
	 */
	public int getLearned() throws SQLException{
		int learned = 0;
		try{
			connect( getUserName(), getPassword() );
			// 現在の学習量を取得
			ResultSet rs = conn.createStatement().executeQuery( "SELECT * FROM tweets ORDER BY id DESC LIMIT 1;" );
			if( rs.next() ) learned = rs.getInt( 1 );
			disconnect();
		}catch( Exception e ){
			e.printStackTrace();
			if( conn != null ){
				conn.rollback();
				conn.close();
			}
		}

		return learned;
	}
	/**
	 * プロフィールの説明文に現在の学習量を追加
	 * @param basic 追加元の説明文
	 * @return 追加後の説明文
	 * @throws SQLException
	 */
	public String addLearned( String basic ) throws SQLException{
		return basic + "\n現在の学習量:" + this.getLearned() + "ツイート";
	}
	/**
	 * 定期処理を行う
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		this.setProfileData();
		this.updateProfile();
		log.info( "プロフィールを自動更新しました。");
	}
	public void test() throws SQLException{
		System.out.println( this.profileName );
		System.out.println( this.url );
		System.out.println( this.location );
		System.out.println( this.addLearned( this.description ) );
	}
}
