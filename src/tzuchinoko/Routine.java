package tzuchinoko;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.TwitterException;
import tzuchinoko.jobs.LogDeleteJob;
import tzuchinoko.jobs.Response;
import tzuchinoko.jobs.UpdateProfileJob;
/**
 * 定期的なツイートなど、定期処理を行うクラス
 * 詳しくは<a href="http://www.quartz-scheduler.org/documentation/quartz-2.2.x/examples">Quartz Scheduler</a>の公式サイトを参照
 * @author hanafusaryo
 *
 */
public class Routine {
	/* ログ出力を行うインスタンス */
	private static final Logger log = LoggerFactory.getLogger( Routine.class );
	/**
	 * 定期処理を行う
	 * 例えば一定時間ごとにbotがツイートを行うのはこのメソッドからである。
	 * 定期処理内容に関してはtzuchinoko.jobsパッケージを参照
	 * 
	 * @param seconds 定期処理開始までの待機時間
	 * @throws SchedulerException
	 * @throws InterruptedException
	 */
	public void run( long seconds ) throws SchedulerException, InterruptedException{
		log.info( "-------Initializing-----------------------" );
		SchedulerFactory sf = new StdSchedulerFactory();
		Scheduler sched = sf.getScheduler();
		log.info( "-------Initialization Complete------------" );

		log.info( "-------Scheduling Jobs--------------------" );
		// cronScheduleの中は 秒 分 時 日 月 ? (年) (曜日)
		/* 定期ツイート */
		JobDetail job = newJob( Response.class ).withIdentity( "job", "response" ).build();
		CronTrigger trigger = newTrigger().withIdentity( "trigger", "response" ).withSchedule( cronSchedule( "22 22 */2 * * ?" ) ).build();
		sched.scheduleJob( job, trigger );
		/* 定期ログファイル削除 */
		job = newJob( LogDeleteJob.class ).withIdentity( "job", "delete_log" ).build();
		//trigger = newTrigger().withIdentity( "trigger", "delete_log" ).withSchedule( cronSchedule( "0 1 0 * * ? * MON" ) ).build();
		trigger = newTrigger().withIdentity( "trigger", "delete_log" ).withSchedule( cronSchedule( "0 58 18 * * ?" ) ).build();
		sched.scheduleJob( job, trigger );
		/* 定期プロフィール更新 */
		job = newJob( UpdateProfileJob.class ).withIdentity( "job", "update_profile" ).build();
		trigger = newTrigger().withIdentity( "trigger", "update_profile" ).withSchedule( cronSchedule( "23 19 5 * * ?" ) ).build();
		sched.scheduleJob( job, trigger );
		
		log.info( "-------Starting Scheduler-----------------" );
		sched.start();
		log.info( "-------Started Scheduler------------------" );

		// 引数文字列がnullでなかったら指定した秒数だけ実行し、その後自動的に終了する
		if( seconds != 0 ){
			log.info( "-------Waiting " + seconds + " seconds...-------------" );
			try{
				Thread.sleep( seconds * 1000L );
			}catch( NumberFormatException e ){
				log.error( "正しい引数を指定してください( Routine#run(String) )", e );
			}
			log.info( "-------Shutting Down----------------------" );
			sched.shutdown( true );
			log.info( "-------Shutdown Complete------------------" );
		}
	}
	/* 主にbotの起動はここから行う */
	public static void main( String[] args ) throws SchedulerException, InterruptedException, TwitterException{
		Thread t = new Thread( new TimelineLoader() );
		t.start();
		Thread.sleep( 3L * 1000L ); // 3秒待つ
		Routine routine = new Routine();
		routine.run( 0 );
	}
}
