package tzuchinoko.jobs;

import java.sql.SQLException;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import tzuchinoko.Dictionary;

/**
 * botの発話クラス。
 * Dictionaryクラスの{@link Dictionary#chain() chain}で生成された文章などをツイートする
 * @author hanafusaryo
 *
 */
public class Response implements Job{
	/* ログ出力を行うインスタンス */
	private static final Logger log = LoggerFactory.getLogger( Response.class );
	/* 文字数制限( 140字以内に設定しておく ) */
	private static final int MAX_LENGTH = 70; 
	/* MAX_LENGTH以内の長さの文章が生成されるまで繰り返す回数 */
	private static final int RETRY = 100; 
	/* Twitterに接続するのに必要 */
	private Twitter twitter;
	/**
	 * Twitterへの接続設定
	 * @throws TwitterException
	 */
	public Response() throws TwitterException{
		twitter = new TwitterFactory().getInstance();
	}
	/**
	 * {@link Dictionary#chain() chain}で生成された文章を取得する
	 * @return botのツイート内容に当たる文字列
	 * @throws SQLException
	 */
	public String respond() throws SQLException{
		String message = "";
		Dictionary dic = new Dictionary();
		boolean b = false;
		for( int i = 0; i < RETRY; i++ ){
			//log.info( "一つ目の文章を生成します。" );
			message = dic.chain();
			if( message.length() <= MAX_LENGTH && dic.isOriginal( message ) ){
				b = true;
				break;
			}
			log.info( "条件を満たしてないため文章の再生成を行います。 (" + (i + 1) + "回目)" );
		}
		if( b == false ){
			log.info( "条件が指定回数以内に満たされなかったので発話を中止します。");
		}
		/*
		else{
			log.info( "発話文章が生成されました。:" + message);
		}
		*/
		return message;
	}
	/*
	public void printRateLimitStatus(){
		try {
			Map<String, RateLimitStatus> rls = twitter.getRateLimitStatus();
			for( String endpoint : rls.keySet() ){
				RateLimitStatus status = rls.get( endpoint );
				log.info( "EndPoint : " + endpoint );
				log.info( "現在の制限回数 : " + status.getLimit() );
				log.info( "API利用回数 : " + status.getRemaining() );
				log.info( "次にリセットされる時間 : " + status.getResetTimeInSeconds() );
				log.info( "リセットされるまでの時間 : " + status.getSecondsUntilReset() + "秒" );
			}
		} catch (TwitterException e) {
			log.error( "RateLimitStatusの取得に失敗しました。", e);
		}
		
	}
	*/
	/**
	 * 定期処理の内容
	 */
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			//log.info( "発話が実行されました。");
			String message = this.respond();
			// 空文字列でなければ発言
			if( !message.equalsIgnoreCase( "" ) ){
				twitter.updateStatus( message );
				log.info( message );
			}else{
				log.info( "発話文章が空白文字列だったので発話を中止します。");
			}
			// API制限を確認
			//this.printRateLimitStatus();
		} catch (SQLException | TwitterException e) {
			log.error( "例外", e);
		}
	}
}
