package tzuchinoko;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.PagableResponseList;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.User;
import twitter4j.UserStreamAdapter;

/**
 * ツイッターからツイートを自動受信して学習するクラス
 * 
 * @author hanafusaryo
 *
 */
public class TimelineLoader extends DataBaseConnection implements Runnable{
	/* ログ出力を行うインスタンス */
	private static final Logger log = LoggerFactory.getLogger( TimelineLoader.class );
	/* 一人につき学習できるツイート数の限度 */
	private static final int MAX_LEARNINGS = 2000; 
	/* twitter4jのPageクラスの持つ１ページ当りのツイート件数 */
	private static final int COUNT = 100;
	/* 内部クラスのインスタンス */
	private UserStreamAnalyzer analyzer;
	/* Twitterに常時接続するユーザストリーム */
	private TwitterStream stream;
	/* Twitterに接続するのに必要 */
	private Twitter twitter;
	/* データベースに接続するのに必要 */
	private Statement stmt = null;
	/**
	 * Twitterに接続しユーザストリームを登楼する
	 * @throws TwitterException
	 */
	public TimelineLoader() throws TwitterException{
		twitter = new TwitterFactory().getInstance();
		// UserStreamのための初期設定
		stream = new TwitterStreamFactory().getInstance();
		analyzer = new UserStreamAnalyzer();
		stream.addListener( analyzer );
	}
	/**
	 * 自動学習開始
	 */
	@Override
	public void run() {
		stream.user();
	}
	/**
	 * 指定したユーザを過去のツイートを学習済みとする
	 * 
	 * @param screenName 対象ユーザのスクリーンネーム
	 * @throws SQLException
	 */
	public void setAppliedUser( String screenName ) throws SQLException{
		try{
			connect( getUserName(), getPassword() );
			stmt = conn.createStatement();
			stmt.executeUpdate( "INSERT INTO targets( screenName ) VALUES( '" + screenName + "' );");
			disconnect();
		}catch( Exception e ){
			e.printStackTrace();
			if( conn != null ){
				conn.rollback();
				conn.close();
			}
		}
	}
	/**
	 * データベースを参照して指定したユーザが学習済みとされているかどうかを返す
	 * 
	 * @param screenName 対象ユーザのスクリーンネーム
	 * @return 最大MAX_LEARNINGS件のツイートを既に学習していればtrueそうでなければfalse
	 * @throws SQLException
	 */
	public boolean isAppliedUser( String screenName ) throws SQLException{
		boolean isApplied = false;
		try{
			connect( getUserName(), getPassword() );
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT * FROM targets WHERE screenName = '" + screenName + "';" );
			isApplied = rs.next();
			disconnect();
		}catch( Exception e ){
			e.printStackTrace();
			if( conn != null ){
				conn.rollback();
				conn.close();
			}
		}
		return isApplied;
	}
	/**
	 * 指定したユーザの最新のツイートをMAX_LEARNINGS件学習する
	 * @param screenName 対象ユーザのスクリーンネーム
	 * @throws SQLException
	 */
	public void learnAllTweets( String screenName ) throws SQLException{
		try{
			Dictionary dic = new Dictionary();
			MorphologicalAnalysis ma = new MorphologicalAnalysis();
			// データベースに接続
			this.connect( this.getUserName(), this.getPassword() );
			// 学習したツイートをテーブルに挿入するSQL文を事前にコンパイル
			PreparedStatement ps = conn.prepareStatement( "INSERT INTO tweets( id, tweet ) VALUES( ?, ? );" );
			// 現在の学習量を取得
			ResultSet rs = conn.createStatement().executeQuery( "SELECT * FROM tweets ORDER BY id DESC LIMIT 1;" );
			int learned = ( rs.next() )? rs.getInt( 1 ) + 1 : 0;
			int page = 1;
			Paging paging = new Paging( page, COUNT );
			User user = twitter.showUser( screenName );
			// 最大学習量 or ツイート件数を超えるまで
			while( page <= ( MAX_LEARNINGS / COUNT) && page <= (user.getStatusesCount() / COUNT) + 1 ){
				ResponseList<Status> rl = twitter.getUserTimeline( screenName, paging);
				for( Status status : rl ){
					String message = status.getText();
					// リツイートは学習しない
					if( isRetweet( message ) ) continue;
					dic.study( ma.analyze( message ) );
					log.debug( "All Learning(@" + screenName + "): " + message );
					ps.setInt( 1, learned); ps.setString( 2,  message );
					ps.addBatch();
					learned++;
				}
				paging.setPage( ++page );
			}
			// addBatch()したSQL文をまとめて実行
			ps.executeBatch();
			log.info( "現在の学習量: " + learned + "ツイート" );
			disconnect();
		}catch( SQLException | TwitterException e ){
			log.error( "データベースかTwitterに接続できません。", e );
			if( conn != null ){
				conn.rollback();
				conn.close();
			}
		}
	}
	/**
	 * 手動でfollowingの任意のユーザからツイートをまとめて取得する
	 * コンソール上にコマンドが出るので、番号を入力して操作する
	 * 
	 * @throws TwitterException
	 * @throws IOException
	 */
	public void select() throws TwitterException, IOException{
		BufferedReader br = new BufferedReader( new InputStreamReader( System.in ) );
		User user = twitter.verifyCredentials();
		PagableResponseList<User> prl = twitter.getFriendsList( user.getScreenName(), -1 );
		int selected = 0;
		do{
			System.out.println( "学習の対象を選択してください" );
			System.out.println( "0 : 終了" );
			for( int i = 0; i < prl.size(); i++ ){
				System.out.println( (i + 1) + " : " + prl.get(i).getScreenName() );
			}
			try{
				selected = Integer.parseInt(  br.readLine() );
			}catch( NumberFormatException e ){
				System.out.println( "整数を入力してください" );
				continue;
			}
			if( selected >= 1 && selected <= prl.size() ){
				try {
					String screenName = prl.get( selected - 1 ).getScreenName();
					if( !this.isAppliedUser( screenName ) ){
						System.out.println( "学習中です。少々お待ちください...." );
						this.learnAllTweets( screenName );
						this.setAppliedUser( screenName );
						log.info( screenName + "のツイートをすべて学習しました。" );
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}while( selected != 0 );
	}
	/**
	 * 最新のツイートを学習する
	 * 
	 * @param status ツイートに関する情報を保持している。詳しくはTwitter4JのAPIドキュメントを参照
	 * @throws SQLException
	 */
	public void learnNewMessage( Status status ) throws SQLException{
		String message = status.getText();
		// リツイートでなければ学習する
		if( !isRetweet( message ) ){
			String screenName = status.getUser().getScreenName();
			log.info( screenName + " : " + message );
			new Dictionary( message );
			// 学習元のツイートを記録
			try {
				this.connect( this.getUserName(), this.getPassword() );
				// 現在の学習量を取得
				ResultSet rs = conn.createStatement().executeQuery( "SELECT * FROM tweets ORDER BY id DESC LIMIT 1;" );
				int learned = ( rs.next() )? rs.getInt( 1 ) + 1 : 0;
				PreparedStatement ps = conn.prepareStatement( "INSERT INTO tweets( id, tweet ) VALUES( ?, ? );" );
				ps.setInt(1, learned); ps.setString( 2, message );
				ps.executeUpdate();
				this.disconnect();
			}catch( SQLException e ){
				log.error( "データベースに接続できません。", e );
				if( conn != null ){
					conn.rollback();
					conn.close();
				}
			}
		}
	}
	/**
	 * 正規表現で引数の文字列がリツイートかどうか判断。Status#isRetweet()と異なり、非公式RTも排除できる
	 * 
	 * @param message 取得したツイートの文字列
	 * @return リツイートならtrueそうでないならfalse
	 */
	private boolean isRetweet( String message ){
		boolean retweet = false;
		Pattern p = Pattern.compile( "(RT|QT) @" );
		Matcher m = p.matcher( message );
		if( m.find() ) retweet = true;
		return retweet;
	}
	/* コンソールから実行するとフォローしたユーザのツイートを待たずにまとめて一度に大量のツイートを学習できる */
	public static void main( String[] args ) throws TwitterException, IOException{
		TimelineLoader tl = new TimelineLoader();
		tl.select();
	}
	/**
	 * botの学習用ユーザストリームクラス
	 * Twitterに接続したまま常に最新のツイートを学習できる
	 * @author hanafusaryo
	 *
	 */
	public class UserStreamAnalyzer extends UserStreamAdapter{
		/* botのユーザ情報 */
		private User user;
		/**
		 * デフォルトコストラクタ
		 * Twitterに接続する
		 * @throws TwitterException
		 */
		public UserStreamAnalyzer() throws TwitterException{
			user = TimelineLoader.this.twitter.verifyCredentials(); 
		}
		/**
		 * botのタイムラインが更新されたらこのメソッドが呼び出される
		 */
		@Override
		public void onStatus( Status status ){
			super.onStatus( status );
			String screenName = status.getUser().getScreenName();
			try{
				// 取得したツイートの発言者がbot自身でなく、リツイートでもなければ学習する
				if( !screenName.equals( user.getScreenName() ) ){
					// followingのユーザが登録済みであれば最新のツイートのみ学習
					if( TimelineLoader.this.isAppliedUser( screenName ) ){
						TimelineLoader.this.learnNewMessage( status );
					}else{// 登録済みでなければすべてのツイートを学習
						TimelineLoader.this.learnAllTweets( screenName );
						log.info( screenName + "のツイートをすべて学習しました。" );
						TimelineLoader.this.setAppliedUser( screenName );
					}
				}
			}catch( SQLException e ){
				log.error( "データベースに接続できませんでした。", e);
			}
		}
	}
}
