package tzuchinoko.jobs;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 定期的にlogディレクトリに保存されるログファイルの削除を行うクラス
 * @author hanafusaryo
 *
 */
public class LogDeleteJob implements Job {
	/* ログ出力を行うインスタンス */
	private static final Logger log = LoggerFactory.getLogger( LogDeleteJob.class );
	/**
	 * 指定した月のログをすべて削除する
	 * 主に先月のログを削除する
	 * @param date 任意の月を指定したCalendarインスタンス
	 */
	public void removePastLogs( Calendar date ){
		int month = date.get( Calendar.MONTH );
		// 月初めの日付にする
		date.set( Calendar.DAY_OF_MONTH, 1 );
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd" );
		// 月初めから月末までの日付のログを削除
		while( date.get( Calendar.MONTH ) == month ){
			String fileName = "./log/tzuchinoko_bot.log." + sdf.format( date.getTime() ) + ".log";
			File file = new File( fileName );
			if( file.exists() ){
				if( file.delete() ){
					log.debug( "ログファイル[" + fileName + "]を削除しました。" );
				}else{
					log.error( "ログファイル[" + fileName + "]の削除に失敗しました。" );
				}
			}else{
				log.debug( "ログファイル[" + fileName + "]が存在しないか見つかりません。");
			}
			// 次の日のログへ
			date.add( Calendar.DAY_OF_MONTH, 1 );
		}
	}
	/**
	 * 一定日数よりも前のログをすべて削除する
	 * removePastLogsは月はじめになると極端にログの量が少なくなるが、このメソッドは常に一定日数のログを保持する
	 * @param days 保持するログの日数(=ログの数)
	 */
	public void keepLimitLogs( int days ){
		Calendar period = Calendar.getInstance();   // 現在時刻を取得
		period.add( Calendar.DAY_OF_MONTH, -days ); // days日前に設定
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd" );
		// ./logディレクトリのログファイルをすべて読み込む
		File dir = new File( "./log" );
		File[] files = dir.listFiles();
		for( File f : files ){

			String fname = f.getName();
			if( fname.equals("tzuchinoko_bot.log") ){ // 日付の付いてない今日のログファイルは除外
				continue;
			}

			// ファイル名からCalendar型に変換
            Calendar dcal = Calendar.getInstance();
            //tzuchinoko_bot.log.yyyy-MM-dd.logのyyyy-MM-ddのところを抽出
            String dstr = fname.substring( "tzuchinoko_bot.log.".length(), "tzuchinoko_bot.log.yyyy-MM-dd".length() );
			try {
				Date date = sdf.parse( dstr );
                dcal.setTime(date);
			} catch (ParseException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			// ファイル名の日付が指定した期間よりも前なら削除する
			if( dcal.before( period ) ){
				if( f.delete() ){
					log.debug( "ログファイル[" + fname + "]を削除しました。" );
				}else{
					log.error( "ログファイル[" + fname + "]の削除に失敗しました。" );
				}
			}else{
				log.debug( fname + "は今日から" + days + "日以前のものではありません。" );
			}

		}
	}
	/**
	 * 定期処理を実行
	 */
	@Override
	public void execute(JobExecutionContext context ) throws JobExecutionException {
		/*
		// 先月のログをすべて削除
		Calendar lastMonth = Calendar.getInstance();
		lastMonth.add( Calendar.MONTH, -1 );
		this.removePastLogs( lastMonth );
		*/
		// 90日以上前のログを削除
		int days = 90;
		this.keepLimitLogs( days );
		log.info( days + "日前のログファイルをすべて削除しました" );
	}
}
