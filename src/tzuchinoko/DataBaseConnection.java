package tzuchinoko;

import java.util.Properties;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tzuchinoko.jobs.Response;

/**
 * データベース接続に関するクラス
 * データベースを操作するクラスではこのクラスを継承する
 * @author hanafusaryo
 *
 */
public class DataBaseConnection {
	/* ログ出力を行うインスタンス */
	private static final Logger log = LoggerFactory.getLogger( Response.class );
	/* MySQLのJDBCドライバクラス名 */
	private final String MySQLJDBCDriver = "com.mysql.jdbc.Driver";
	/* JDBCドライバクラス名 */
	private final String JDBCDriver = MySQLJDBCDriver;
	/* 使用するデータベースのスキーマ */
	private final String DBNAME;
	/* データベースに接続するユーザ名 */
	private final String USER;
	/* データベースに接続するためのユーザパスワード */
	private final String PASS;
	/* データベースの場所 */
	private final String DBURL;
	/* データベース接続に必要 */
	protected Connection conn = null;
	/**
	 * 設定ファイルconnection.propertiesからデータベースの接続に必要な情報を読み込む
	 */
	public DataBaseConnection(){
		Properties prop = new Properties();
		try {
			prop.load( new FileInputStream( "./src/connection.properties" ) );
		} catch (IOException e) {
			log.error( "connection.propertiesからデータベース接続に関する設定が読み込めません", e );
		}
		this.DBNAME = prop.getProperty( "connection.dbname" );
		this.USER = prop.getProperty( "connection.user" );
		this.PASS = prop.getProperty( "connection.password" );
		this.DBURL = "jdbc:mysql://localhost/" + DBNAME;
	}
	/**
	 * ユーザ名を取得する
	 * @return ユーザ名
	 */
	protected String getUserName(){
		return USER;
	}
	/**
	 * パスワードを取得する
	 * @return パスワード
	 */
	protected String getPassword(){
		return PASS;
	}
	/**
	 * データベースへの接続
	 * @param user ユーザ名
	 * @param pass パスワード
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	protected void connect( String user, String pass )throws SQLException{
		// JDBCドライバを読み込む
		try {
			Class.forName( JDBCDriver ).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			log.error( "JDBCドライバの読み込みに失敗しました。", e );
		}
		try{
			// 接続
			if( user == null && pass == null ){
				conn = DriverManager.getConnection( DBURL );
			}else{
				Properties prop = new Properties();
				prop.put( "user", user );
				prop.put( "password", pass );
				prop.put( "characterEncoding", "utf8" );
				conn = DriverManager.getConnection( DBURL, prop );
			}
		}catch( Exception e ){
			log.error( "データベースへの接続に失敗しました。", e );
			if( conn != null ){
				conn.rollback();
				conn.close();
			}
		}
	}
	/**
	 * データベースとの接続終了
	 * @throws SQLException
	 */
	protected void disconnect() throws SQLException{
		try{
			conn.close();
		}catch( Exception e ){
			log.error( "データベース接続の切断に失敗しました。", e );
			if( conn != null ){
				conn.rollback();
				conn.close();
			}
		}
	}
}

