package tzuchinoko;

import java.io.File;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.ArrayList;
import java.util.List;
import org.chasen.mecab.Tagger;
import org.chasen.mecab.Node;

/**
 * 形態素解析を扱うクラス
 * 形態素解析を行うことで日本語のような分ち書きが必要な文章を品詞ごとに分解することができる.
 * @author hanafusaryo
 *
 */
public class MorphologicalAnalysis {
	/**
	 * 形態素解析用の辞書の読み込み
	 */
	static{
		try {
			File f = new File( "/home/hanafsary/mecab-java-0.996/libMeCab.so" );
            System.load( f.toString() );
        } catch (UnsatisfiedLinkError e) {
        	System.err.println("Cannot load the example native code.\nMake sure your LD_LIBRARY_PATH contains \'.\'\n" + e);
            System.exit(1);
        }
	}
	/**
	 * 引数の文字列を形態素解析してArrayListに入れて渡す。最後の要素に終端文字を入れる。
	 * 
	 * @param message 形態素解析を行う対象の文字列
	 * @return 形態素解析によって品詞ごとに分けらてた文字列のリスト
	 */
	public List<String> analyze( String message ){
		List<String> morphemes = new ArrayList<String>();
		Tagger tagger = new Tagger();
		message = removeParticularWords( message );
		// 改行ごとに分けて配列に収める
		String[] sentences = message.split( "\n" );
		for( int i = 0; i < sentences.length; i++ ){
			// 配列の中の文字列を形態素解析
			tagger.parse( sentences[i] );
			// 形態素解析したものをnodeに格納
			Node node = tagger.parseToNode( sentences[i] );
			while( node != null ){
				// 形態素の品詞などの特徴を配列に収める
				String[] features = node.getFeature().split( "," );
				// surfaceが空文字となる特殊な形態素は飛ばす(本来文頭と文末を表すもの)
				if( !features[0].equals( "BOS/EOS" ) ){
					morphemes.add( node.getSurface() );
				}
				node = node.getNext();
			}
			// 一番最後だったら終端文字、そうでなかったら改行文字を入れる。
			if( i == sentences.length - 1 ){
				morphemes.add( "[end]" );
			}else{
				morphemes.add( "[nextLine]" );
			}
		}
		return morphemes;
	}
	/**
	 * リプライやURLなどの特殊な文字列を除外する。
	 * 
	 * @param message
	 * @return 学習に必要のない文字列が除外された文字列
	 */
	private String removeParticularWords( String message ){
		String processed = message;
		Pattern p = Pattern.compile( "(RT|QT) @" );
		Matcher m = p.matcher( processed );
		if( m.find() ){// リツイート文は学習しないので空文字列を格納する
			processed = "";
		}else{
			// リプライを除外
			p = Pattern.compile( "@[A-Za-z0-9_]+[\\s　]?" );
			m = p.matcher( processed );
			processed = m.replaceAll( "" );
			// ハッシュタグを除外
			p = Pattern.compile( "(?:^|[^ｦ-ﾟー゛゜々ヾヽぁ-ヶ一-龠ａ-ｚＡ-Ｚ０-９a-zA-Z0-9&_\\/]+)[#＃]([ｦ-ﾟー゛゜々ヾヽぁ-ヶ一-龠ａ-ｚＡ-Ｚ０-９a-zA-Z0-9_]*[ｦ-ﾟー゛゜々ヾヽぁ-ヶ一-龠ａ-ｚＡ-Ｚ０-９a-zA-Z]+[ｦ-ﾟー゛゜々ヾヽぁ-ヶ一-龠ａ-ｚＡ-Ｚ０-９a-zA-Z0-9_]*)" );
			m = p.matcher( processed );
			processed = m.replaceAll( "" );
			// URLを除外
			p = Pattern.compile( "https?://[\\w/:%#\\$&\\?\\(\\)~\\.=\\+\\-]+" );
			m = p.matcher( processed );
			processed = m.replaceAll( "" );
		}
		return processed;
	}
}
